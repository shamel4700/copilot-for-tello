package com.shamel.copilotfordji;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.arthenica.ffmpegkit.FFmpegKit;
import com.google.android.material.snackbar.Snackbar;
import com.shamel.copilotfordji.databinding.ActivityMainBinding;
import com.zerokol.views.joystickView.JoystickView;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import tellolib.camera.TelloCamera;
import tellolib.control.TelloControl;
import tellolib.drone.TelloDrone;


public class MainActivity extends AppCompatActivity {

    //<editor-fold desc="permissions">
    //permission code obtained and modified from DJI tutorial:
    //      https://developer.dji.com/mobile-sdk/documentation/android-tutorials/index.html
    private static final String[] REQUIRED_PERMISSION_LIST = new String[]{

            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE

    };
    private final List<String> missingPermission = new ArrayList<>();
    private static final int REQUEST_PERMISSION_CODE = 123;
    //</editor-fold>

    //<editor-fold desc="drone variables and threads">
    ActivityMainBinding Binding;

    //joystick variables
    private int roll = 0;
    private int pitch = 0;
    private int yaw = 0;
    private int throttle = 0;
    JoystickView leftStick;
    JoystickView rightStick;

    //tello class varibles
    private TelloControl telloControl;
    private TelloCamera telloCamera;
    private TelloDrone telloDrone;
    private String startTime;
    private String endTime;

    //logging variables
    private static final Logger 		logger = Logger.getGlobal();
    private static final ConsoleHandler handler = new ConsoleHandler();

    //flags for drone connections, camera feed and recording
    private boolean connectFlag;
    private boolean cameraActive;
    private boolean recording;

    //threads and thread handlers
    HandlerThread network = new HandlerThread("network");
    HandlerThread camera = new HandlerThread("camera");
    Handler networkHandler;
    Handler cameraHandler;
    Handler mainHandler = new Handler(Looper.getMainLooper());
    Thread flightThread;
    Thread infoThread;
    Thread facesThread;

    //list to hold views for enable/disable operations
    List<View> viewList = new ArrayList<>();

    //variables for drone status
    int battery = 0, speed = 0, height = 0, temp = 0;
    double baro = 0;

    //</editor-fold>

    static {
        if(OpenCVLoader.initDebug()){
            Log.e("Copilot","OpenCV loaded.");
        }else{
            Log.e("Copilot","OpenCV failed to load.");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //bind main layout and check for  required permissions
        Binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        checkAndRequestPermissions();

        //initialise flags
        connectFlag=false;
        cameraActive=false;
        recording=false;

        //set application wide error handling to prevent crashes
        Thread.setDefaultUncaughtExceptionHandler((paramThread, paramThrowable) -> {
            Log.e("Copilot","App crashed");
            Snackbar.make(Binding.MainLayout,"Crash Avoided", Snackbar.LENGTH_LONG).show();
        });

        //<editor-fold desc="logging, tello variable initialising and button list">

        //set up logging
        logger.addHandler(handler);
        logger.setUseParentHandlers(false);
        logger.setLevel(Level.FINEST);
        handler.setLevel(Level.FINEST);
        logger.info("start");

        //initialise tello class variables
        telloControl = TelloControl.getInstance();
        telloControl.setLogLevel(Level.FINEST);
        telloCamera =TelloCamera.getInstance();
        telloCamera.setViewAndHandler(Binding.CameraFeed,mainHandler);

        //add views to list for enable/disable
        viewList.add(Binding.Shutdown);
        viewList.add(Binding.Camera);
        viewList.add(Binding.Picture);
        viewList.add(Binding.Record);

        viewList.add(Binding.rightStick);
        viewList.add(Binding.leftStick);

        viewList.add(Binding.Takeoff);
        viewList.add(Binding.Land);

        //</editor-fold>

        //<editor-fold desc="joystick setup">
        leftStick = Binding.leftStick;
        rightStick = Binding.rightStick;

        rightStick.setOnJoystickMoveListener((angle, power, direction) -> {
            //calculate roll and pitch from joystick values
            roll = (int) (power * Math.sin(angle * (Math.PI/180)));
            pitch = (int) (power * Math.cos(angle * (Math.PI/180)));
        }, JoystickView.DEFAULT_LOOP_INTERVAL);//100ms interval for checking input

        leftStick.setOnJoystickMoveListener((angle, power, direction) -> {
            //calculate yaw and throttle from joystick values
            yaw = (int) (power * Math.sin(angle * (Math.PI/180)));
            throttle = (int) (power * Math.cos(angle * (Math.PI/180)));
        }, JoystickView.DEFAULT_LOOP_INTERVAL);

        //</editor-fold>

        //<editor-fold desc="threads">

        //start network and camera operation threads
        network.start();
        networkHandler = new Handler(network.getLooper());
        camera.start();
        cameraHandler = new Handler(camera.getLooper());

        //define drone info thread
        infoThread = new Thread(()->{
            while(true){
                try {//update status bar with new info
                    if(telloDrone.isConnected()){
                        Binding.StatusConnect.setTextColor(getColor(R.color.green));
                        Binding.StatusConnect.setText("CONNECTED");
                        Binding.StatusBattery.setText("BATTERY: " + battery +"%");
                    }else{//set default text
                        Binding.StatusConnect.setTextColor(getColor(R.color.red));
                        Binding.StatusConnect.setText(getText(R.string.connect_default));
                        Binding.StatusBattery.setText(getText(R.string.battery_default));
                    }
                    if(telloDrone.isFlying()){
                        Binding.StatusAltitude.setText("ALTITUDE: " + height + "cm");
                    }else{
                        Binding.StatusAltitude.setText(getText(R.string.altitude_default));
                    }
                    updateInfo(telloControl);
                    Thread.sleep(200);
                }catch (Exception e){
                    //Log.e("Copilot", e.getMessage());
                }
            }
        });

        infoThread.start();

        //use separate thread for joystick inputs
        flightThread = new Thread(()->{
            while (!Thread.interrupted()){
                try {
                    telloControl.flyRC(roll,pitch,throttle,yaw);
                } catch (Exception e) {
                    Log.e("Copilot","joystick",e);
                    if(telloDrone.isFlying()) telloControl.land();
                }
            }
        });

        facesThread = new Thread(()->{
            Binding.Faces.setVisibility(View.VISIBLE);
            while(cameraActive) {
                try {
                    Bitmap bmp = Bitmap.createBitmap(Binding.CameraFeed.getWidth(), Binding.CameraFeed.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bmp);
                ArrayList<Rect> targets = telloCamera.getTarget();
                if(targets != null) {
                    Rect target = targets.get(0);
                    int x = target.x;
                    int y = target.y;
                    int w = target.width;
                    int h = target.height;

                    ShapeDrawable rect = new ShapeDrawable(new RectShape());
                    rect.setBounds(x, y, x + w, y + h);
                    rect.getPaint().setColor(Color.parseColor("#ff0000"));
                    rect.getPaint().setStyle(Paint.Style.STROKE);
                    rect.draw(canvas);

                    mainHandler.post(() -> {//draw rectangles
                        Binding.Faces.setImageBitmap(bmp);
                    });


                }else{
                    canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    mainHandler.post(() -> {//clear rectangles when no target
                        Binding.Faces.setImageBitmap(bmp);
                    });
                }
                    Thread.sleep(50); }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Binding.Faces.setVisibility(View.INVISIBLE);
        });

        //</editor-fold>

        //disable all buttons except connect button before connecting
        disableButtons();
        //set clicklisterners for the UI buttons
        setListeners();
    }

    //<editor-fold desc="delete images and video folders">

    //Note 1: Since the camera feed works by storing pictures from
    //the drone and displaying them sequentially, it is
    //possible that when the app runs in the background
    //with the camera feed on, images will get stored
    //but not displayed. This can cause a large delay
    //in the feed. Likewise when the app is stopped,
    //the remaining images do not get displayed and
    //take up useful space. Hence every time the app is
    //pause or stopped, the image folder is deleted to
    //prevent it from taking up useful space and also
    //syncing up the camera feed to the drone.

    //Note 2: While recording a video, frames are stored
    //in a separate folder to later be converted to a video.
    //If the app gets paused or stopped when not recording
    //the folder must be deleted to not take valuable space.
    @Override
    protected void onStop(){
        deleteFolder("images");
        if(!recording)deleteFolder("video");
        super.onStop();
    }


    //</editor-fold>

    //set listeners for buttons
    private void setListeners(){
        //Connect Button clicklistener
        Binding.Connect.setOnClickListener(view ->  {
            touchFeedback(view);
            try
            {
                if(!connectFlag){// if not connected
                    //use network thread for network operations
                    networkHandler.post(() -> {
                        try{
                            telloControl.connect();//connect to drone
                            telloControl.enterCommandMode();

                            //run UI changes on main thread
                            mainHandler.post(() -> {
                                //show connect message
                                Snackbar.make(Binding.MainLayout,"Connected!",
                                        Snackbar.LENGTH_SHORT).show();

                                //enable takeoff, shutdown and camera
                                enableView(Binding.Takeoff);
                                enableView(Binding.Shutdown);
                                enableView(Binding.Camera);

                                connectFlag =true;
                                telloDrone = TelloDrone.getInstance();
                            });

                        }catch (Exception e){
                            Snackbar.make(Binding.MainLayout,e.getMessage(),
                                    Snackbar.LENGTH_LONG).show();
                        }
                    });
                }else{//disconnect
                    //on disconnect disable camera if active
                    if(cameraActive){
                        disableCamera();
                    }

                    //stop flight inputs and land if flying
                    flightThread.interrupt();
                    networkHandler.post(()->{
                        if(telloDrone.isFlying()) telloControl.land();
                        telloControl.disconnect();
                    });
                    Snackbar.make(Binding.MainLayout,"Disconnected!",
                            Snackbar.LENGTH_SHORT).show();
                    touchFeedback(view);
                    disableButtons();//disable buttons when disconnected
                    connectFlag =false;
                }
            }
            catch (Exception e) {
                Log.e("Copilot","connection",e);
            }
        });

        //Camera Button clicklistener
        Binding.Camera.setOnClickListener(view -> {
            if(cameraActive){
                disableCamera();
            }else{
                //set camera feed frame size to match app layout dimensions
                telloCamera.setScale(Binding.MainLayout.getMeasuredWidth(),
                        Binding.MainLayout.getMeasuredHeight());

                //disable button for 5 seconds
                view.setEnabled(false);
                mainHandler.postDelayed(()->view.setEnabled(true),5000);
                Snackbar.make(Binding.MainLayout, "Starting camera feed...Wait 5 seconds",
                        Snackbar.LENGTH_LONG).show();

                //perform camera operations on the camera thread
                cameraHandler.post(()->{
                    try{
                        //turn drone camera on and connect to it
                        telloControl.streamOn();
                        telloCamera.startVideoCapture(MainActivity.this);

                        //enable picture cap and record buttons
                        mainHandler.post(()->{
                            enableView(Binding.Picture);
                            enableView(Binding.Record);
                        });

                    }catch(Exception e){
                        Log.e("Copilot","enable camera",e);
                        if(telloDrone.isFlying()) telloControl.land();
                    }});
                cameraActive=true;
                facesThread.start();
            }
            touchFeedback(Binding.Camera);
        });

        //Takeoff Button clicklistener
        Binding.Takeoff.setOnClickListener(view -> {
            networkHandler.post(()->{
                try{
                    //take off and enable relevant UI elements
                    telloControl.takeOff();
                    mainHandler.post(()->{
                        flightThread.start();//start joystick input thread
                        enableView(Binding.rightStick);
                        enableView(Binding.leftStick);
                        enableView(Binding.Land);
                    });

                }catch(Exception e){
                    Log.e("Copilot","takeoff",e);
                    if(telloDrone.isFlying()) telloControl.land();
                }
            });
            touchFeedback(Binding.Takeoff);
        });

        //Land Button clicklistener
        Binding.Land.setOnClickListener(view -> {
            flightThread.interrupt();
            networkHandler.removeCallbacksAndMessages(null);
            networkHandler.post(()-> {
                if (telloDrone.isFlying()) {//land if flying
                    telloControl.land();
                    mainHandler.post(() -> {
                        disableButtons();

                        //show landing message
                        Snackbar.make(Binding.MainLayout, "Landing...",
                                Snackbar.LENGTH_SHORT).show();

                        //enable takeoff, land, shutdown and camera
                        enableView(Binding.Takeoff);
                        enableView(Binding.Shutdown);
                        enableView(Binding.Camera);
                    });

                } else {//otherwise show message
                    Snackbar.make(Binding.MainLayout, "Already landed.",
                            Snackbar.LENGTH_SHORT).show();
                }
            });
            touchFeedback(Binding.Land);
        });

        //Shutdown Button clicklistener
        Binding.Shutdown.setOnClickListener(view -> {
            flightThread.interrupt();
            networkHandler.removeCallbacksAndMessages(null);
            networkHandler.post(()->{
                try {
                    telloControl.emergency();
                    mainHandler.post(() -> {
                        disableButtons();

                        //enable takeoff, shutdown and camera
                        enableView(Binding.Takeoff);
                        enableView(Binding.Shutdown);
                        enableView(Binding.Camera);
                    });
                }catch(Exception e){
                    Log.e("Copilot","emergency",e);
                    if(telloDrone.isFlying()) telloControl.land();
                }
            });
            touchFeedback(Binding.Shutdown);
        });

        //Picture-Cap Button clicklistener
        Binding.Picture.setOnClickListener(view -> {
            touchFeedback(view);

            //disable button for 5 seconds
            view.setEnabled(false);
            mainHandler.postDelayed(() -> view.setEnabled(true), 5000);

            cameraHandler.post(() -> {
                try {
                    telloCamera.takePicture(this);
                } catch (Exception e) {
                    Log.e("Copilot", "screenshot main", e);
                }
            });
        });

        //Record Button clicklistener
        Binding.Record.setOnClickListener(view -> {
            touchFeedback(view);
            if(recording){
                Snackbar.make(Binding.MainLayout, "Stopping recording",
                        Snackbar.LENGTH_SHORT).show();
                recording=false;
                stopRecording();
            }else{
                deleteFolder("video");
                recording = true;

                //disable button for 5 seconds
                view.setEnabled(false);//disable button for 5 seconds
                mainHandler.postDelayed(()->view.setEnabled(true),5000);

                try {
                    telloCamera.startRecording(this);
                    startTime = new SimpleDateFormat("mmss").format(new Date());

                    mainHandler.postDelayed(()->{//conclude recording after 30 seconds
                        if(recording){
                            Snackbar.make(Binding.MainLayout, "Stopping recording",
                                    Snackbar.LENGTH_SHORT).show();
                            stopRecording();
                        }
                    },30000);

                } catch (Exception e) {
                    Log.e("Copilot", "record start main", e);
                }
            }
        });
    }

    private void updateInfo(TelloControl telloControl) {
        battery = telloControl.getBattery();
        speed = telloControl.getSpeed();
        baro = telloControl.getBarometer();
        height = telloControl.getHeight();
        temp = telloControl.getTemp();
        //System.out.println(battery + ", " + speed  + ", " + baro + ", " + height + ", " + temp);
    }

    //<editor-fold desc="UI and system helper functions that may need to be used more than once">
//    private void enableButtons(){
//        for(View view :viewList){
//                enableView(view);
//        }
//    }

    private void disableButtons(){
        for(View view:viewList){
            disableView(view);
        }
        if(cameraActive){
            enableView(Binding.Picture);
            enableView(Binding.Record);
        }
    }

    private void touchFeedback(View view){//provide touch respond from buttons
        view.setAlpha((float) 0.3);
        new Timer().schedule(new TimerTask(){
            @Override
            public void run() {
                view.setAlpha(1);
            }
        },100);
    }

    private void enableView(View view){
        view.setEnabled(true);
        view.setAlpha(1);
    }

    private void disableView(View view){
        view.setEnabled(false);
        view.setAlpha((float)0.2);
    }

    private void deleteFolder(String folder){
        String internalPath = getExternalFilesDirs(null)[0].getAbsolutePath();
        File imageDir = new File(internalPath,folder);
        if (imageDir.isDirectory()) {
            String[] files = imageDir.list();
            if (files != null) {
                for (String file : files) {
                    new File(imageDir, file).delete();
                }
            }
        }
    }

    private void disableCamera(){//stop camera feed
        if(recording) stopRecording();
        disableView(Binding.Picture);
        disableView(Binding.Record);

        //disable button for 5 seconds
        Binding.Camera.setEnabled(false);
        mainHandler.postDelayed(() -> Binding.Camera.setEnabled(true), 5000);

        Snackbar.make(Binding.MainLayout, "Stopping camera feed...Wait 5 seconds",
                Snackbar.LENGTH_LONG).show();
        cameraHandler.post(() -> {
            try {
                telloControl.streamOff();
            } catch (Exception e) {
                Log.e("Copilot", "disable camera", e);
                if(telloDrone.isFlying()) telloControl.land();
            }
        });
        telloCamera.stopVideoCapture(this);
        cameraActive = false;
    }

    private void stopRecording(){//stop video recording
        Binding.Record.setEnabled(false);//disable button for 5 seconds
        mainHandler.postDelayed(()->{
            if(cameraActive)Binding.Record.setEnabled(true);
        },5000);

        try {
            telloCamera.stopRecording(this);
            endTime = new SimpleDateFormat("mmss").format(new Date());
            String vidPath = getExternalFilesDirs(null)[0].getAbsolutePath()
                    + "/video/";
            int totalFrames = new File(vidPath).listFiles().length;
            int fps = totalFrames/calcTime(startTime,endTime);
            new Thread(()->{
                String[] cmd = {"-start_number","1","-r", String.valueOf(fps),"-i",
                        vidPath+ "frame_%d.jpg" ,"-y",vidPath +"video.mp4"};
                FFmpegKit.executeWithArguments(cmd);
                System.out.println(startTime  + "   "  + endTime + "    "
                        + calcTime(startTime,endTime));
                //System.out.println(totalFrames);
                //System.out.println(fps);
                saveToGallery();//save video to gallery
                runOnUiThread(()->Snackbar.make(Binding.Record, "Video stored in Gallery.",
                        Snackbar.LENGTH_SHORT).show());
            }).start();

        } catch (Exception e) {
            Log.e("Copilot", "record stop main", e);
            Snackbar.make(Binding.Record, "Video creation failed.",
                    Snackbar.LENGTH_SHORT).show();
        }
    }

    private int calcTime(String start,String end){//calc time between two mmss times
        int startMin = Integer.parseInt(start.substring(0,2));
        int startSec = Integer.parseInt(start.substring(2,4));
        int endMin = Integer.parseInt(end.substring(0,2));
        int endSec = Integer.parseInt(end.substring(2,4));
        if (startMin>endMin)endMin+=60;//account for wrap arounds
        if (startSec>endSec){
            endSec+=60;
            startMin+=1;
        }
        return (endMin-startMin)*60 + (endSec-startSec);
    }
    //</editor-fold>

    //save video to gallery
    private void saveToGallery(){
        //code adapted from : https://stackoverflow.com/questions/57923329/
        //                                  save-and-insert-video-to-gallery-on-android-10
        Uri uri = null;
        ContentResolver resolver =  getContentResolver();
        try {
            String vidPath = getExternalFilesDirs(null)[0].getAbsolutePath() + "/video/";
            String dateAndTime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String videoFileName = "TelloVid_" + dateAndTime;

            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.Video.Media.DISPLAY_NAME, videoFileName);
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "video/mp4");
            if(Build.VERSION.SDK_INT >= 29){
                uri = resolver.insert(MediaStore.Video.Media.getContentUri
                        (MediaStore.VOLUME_EXTERNAL_PRIMARY), contentValues);
            }else{
                uri = resolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, contentValues);
            }
            ParcelFileDescriptor pfd;
            try {
                pfd = getContentResolver().openFileDescriptor(uri, "w");
                FileOutputStream out = new FileOutputStream(pfd.getFileDescriptor());
                FileInputStream in = new FileInputStream(vidPath+"video.mp4");

                byte[] buf = new byte[8192];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                out.close();
                in.close();
                pfd.close();

                deleteFolder("video");
                mainHandler.post(()-> Snackbar.make(Binding.MainLayout,
                        "Video saved to Gallery", Snackbar.LENGTH_LONG).show());
            } catch (Exception e) {
                e.printStackTrace();
                mainHandler.post(()-> Snackbar.make(Binding.MainLayout,
                        "Video creation failed", Snackbar.LENGTH_LONG).show());
            }
        }
        catch(Exception e){
            Log.e("Copilot","video",e);
            if (uri != null) {
                resolver.delete(uri, null, null);
            }
            Snackbar.make(Binding.MainLayout, "Video Creation Failed",
                    Snackbar.LENGTH_LONG).show();
        }
    }

    //<editor-fold desc="permissions">
    private void checkAndRequestPermissions() {
        // Check for permissions
        for (String eachPermission : REQUIRED_PERMISSION_LIST) {
            if (ContextCompat.checkSelfPermission(this, eachPermission) !=
                    PackageManager.PERMISSION_GRANTED) {
                missingPermission.add(eachPermission);
            }
        }
        // Request for missing permissions
        if (!missingPermission.isEmpty() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    missingPermission.toArray(new String[missingPermission.size()]),
                    REQUEST_PERMISSION_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Check for granted permission and remove from missing list
        if (requestCode == REQUEST_PERMISSION_CODE) {
            for (int i = grantResults.length - 1; i >= 0; i--) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    missingPermission.remove(permissions[i]);
                }
            }
        }
        // If there is enough permission, we will start the registration
        if (!missingPermission.isEmpty()) {
            Toast.makeText(this, "Missing permissions!!!", Toast.LENGTH_LONG).show();
        }
    }
    //</editor-fold>

}