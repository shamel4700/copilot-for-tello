package tellolib.camera;
//Modified to work with Android.
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.arthenica.ffmpegkit.FFmpegKit;
import com.arthenica.ffmpegkit.FFmpegSession;
import com.arthenica.ffmpegkit.ReturnCode;
import com.arthenica.ffmpegkit.SessionState;
import com.google.android.material.snackbar.Snackbar;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.function.Supplier;
import java.util.logging.Logger;
/**
 * Convenience functions for Tello camera.
 */
public class TelloCamera implements TelloCameraInterface
{
	private final 					Logger logger = Logger.getLogger("Tello");
	private FFmpegSession 			feedSession;
	private File 					recordDir;
	private Thread					recordThread;
	private int 					frameNum=0;
	private boolean					recording;
	private File 					outputImg;
	private Thread					videoCaptureThread;
	private Thread					faces;
	private Mat						image;
	private String 					scale;
	private Handler 				mainHandler;
	private Size					videoFrameSize ;//size of video frame
	private double					videoFrameRate = 30;
	private SimpleDateFormat		df = new SimpleDateFormat("yyyy-MM-dd.HHmmss");
	private String					statusBar = null;
	private Supplier				<String>statusBarMethod = null;
	private Object					lockObj = new Object();
	private ArrayList<Rect>			targetRectangles;
	private ArrayList<MatOfPoint>	contours = null;
	private int						targetWidth = 1, contourWidth = 1;
	private ImageView 				cameraFeedWindow;
	private Scalar 					targetColor = new Scalar(0, 0, 255), contourColor = new Scalar(255, 0, 0);
	private FaceDetection			faceDetection;

	private TelloCamera()
	{
//		System.loadLibrary("opencv_java4");
//		// Load OpenCV library.
//		//OpenCVLoader.initDebug();
//		//System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		image = new Mat();
	}
	public void setViewAndHandler(ImageView view,Handler handler){
		mainHandler=handler;
		cameraFeedWindow=view;
	}

	private static class SingletonHolder
	{
		public static final TelloCamera INSTANCE = new TelloCamera();
	}
	
	/**
	 * Get the global instance of TelloCamera.
	 * @return Global TelloCamera instance.
	 */
	public static TelloCamera getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	@Override
	public void startVideoCapture(Context context)
	{
		faceDetection = FaceDetection.getInstance();
		faceDetection.setCascades(context);//load required Haar cascades
		faces = new Thread(()->{
			while(!faces.isInterrupted()){
				try {
					boolean found = faceDetection.detectFaces();
					if (found)
					{
						// Get the array of rectangles describing the location and size
						// of the detected faces.
						Rect[] faces = faceDetection.getFaces();

						// Clear any previous target rectangles.
						addTarget(null);

						// Set first face rectangle to be drawn on video feed.
						addTarget(faces[0]);
					}else{
						// Clear any previous target rectangles.
						addTarget(null);
					}
					Thread.sleep(200);
				} catch (Exception e) {
					//e.printStackTrace();
				}
			}
		});
		logger.fine("starting video capture");
		String internalPath = context.getExternalFilesDirs(null)[0].getAbsolutePath();

		//delete image folder if exists
		deleteFolder(context,"images");

		//create images and video directory and sequential image file
		new File(context.getExternalFilesDirs(null)[0],"images").mkdir();
		new File(context.getExternalFilesDirs(null)[0],"video").mkdir();
		outputImg = new File(internalPath+"/images/image_%d.jpg");

		//streaming links for tello camera, and test webcam
		String cameraUDP = "udp://0.0.0.0:11111";
		String test = "http://192.168.0.4:8081/video.mjpg";

		//build ffmpeg command
		String[] cmd1 = {"-probesize" ,"32","-analyzeduration","0","-flags","low_delay","-i",cameraUDP,
				"-r","10","-vf",scale,"-y",outputImg.getAbsolutePath()};

		//ffmpeg session for camera feed
		feedSession = FFmpegKit.executeWithArgumentsAsync(cmd1, session -> {
			SessionState state = session.getState();
			ReturnCode returnCode = session.getReturnCode();
			Log.e("Copilot", String.format("FFmpeg process exited with state %s and rc %s.%s"
					, state, returnCode, session.getFailStackTrace()));
		});
		videoCaptureThread = new VideoCaptureThread(context);
		videoCaptureThread.start();
		faces.start();
	}

	@Override
	public void stopVideoCapture(Context context)
	{
		if (videoCaptureThread != null) {
			logger.fine("stopping video capture thread");
			try {// Signal threads to stop.
				System.out.println("cancelling");
				videoCaptureThread.interrupt();
				feedSession.cancel();
				faces.interrupt();
				System.out.println("cancelled");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("exit");
			}
		}
		//reset layout background to white
		mainHandler.post(()->cameraFeedWindow.setVisibility(View.INVISIBLE));
		//delete images folder so remaining images do not take up space
		deleteFolder(context,"images");
	}
	
	@Override
	public Mat getImage()
	{

		Bitmap bmp = ((BitmapDrawable) cameraFeedWindow.getDrawable()).getBitmap();
		Utils.bitmapToMat(bmp,image);
		return image.clone();
	}

	//thread to read the images of the video stream and process them
	private class VideoCaptureThread extends Thread
	{
		private Context appContext;
		VideoCaptureThread(Context context)
		{
			logger.fine("video thread constructor");
			appContext=context;
			this.setName("VideoCapture");
		}
		
		public void run()
		{
			logger.fine("video capture thread started");
			try
			{
				mainHandler.post(()->cameraFeedWindow.setVisibility(View.VISIBLE));
				int imageNum = 1;

				while (!videoCaptureThread.isInterrupted())
				{
					try {
						//get frame from folder
						File myImage = new File(appContext.getExternalFilesDirs(null)[0].
								getAbsolutePath()+"/images/image_"+imageNum+".jpg");

						while(!myImage.exists()){}//halt this thread until image is created
						//display frame
						mainHandler.post(()->
								cameraFeedWindow.setImageBitmap(BitmapFactory.decodeFile(myImage.getAbsolutePath())));
						imageNum++;

					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			catch (Exception e)
			{
				logger.severe("video capture failed: " + e.getMessage());
				deleteFolder(appContext,"images");
			}
			finally {}
			logger.fine("Video capture thread ended");
			videoCaptureThread = null;
		}
	}

	@Override
	public void takePicture(Context context)
		{
		//code adapted from: https://stackoverflow.com/questions/63776744/
		// 				save-bitmap-image-to-specific-location-of-gallery-android-10
		//and: https://stackoverflow.com/questions/38229257/
		// 				converting-an-image-file-to-a-png-on-android
		Uri uri = null;
		ContentResolver resolver =  context.getContentResolver();
		try {
			String dateAndTime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
			String imageFileName = "TelloPic_" + dateAndTime;

			ContentValues contentValues = new ContentValues();
			contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, imageFileName);
			contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
			uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

			BitmapDrawable bmp = (BitmapDrawable) cameraFeedWindow.getDrawable();
			OutputStream outStream = resolver.openOutputStream(uri);
			bmp.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, outStream);
			Snackbar.make(cameraFeedWindow, "Image Stored In Gallery",
					Snackbar.LENGTH_LONG).show();
		}catch(Exception e){
			Log.e("Copilot","screenshot",e);
			if (uri != null) {
				resolver.delete(uri, null, null);
			}
			Snackbar.make(cameraFeedWindow, "Image Capture Failed",
					Snackbar.LENGTH_LONG).show();
		}
	}

	@Override
	public void startRecording( Context context )
	{
		recording=true;
		frameNum = 1;
		//create directory for video frames
		recordDir = new File(context.getExternalFilesDirs(null)[0],"video");
		recordThread = new Thread(()->{
			while(!recordThread.isInterrupted()){
				String imageName = "frame_"+frameNum+".jpg";
				File myImage = new File(recordDir,imageName);
				try {//get frame from camera feed window and store it as jpg
					Bitmap myBitmap = ((BitmapDrawable) cameraFeedWindow.getDrawable()).getBitmap();
					FileOutputStream fileOutputStream = new FileOutputStream(myImage.getAbsolutePath());
					myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
					fileOutputStream.close();
					frameNum++;
				} catch (Exception e) {
					//e.printStackTrace();
				}
			}
		});
		recordThread.start();
	}

	@Override
	public void stopRecording(Context context)
	{
		recording =  false;
		recordThread.interrupt();

	}


	@Override
	public void addTarget( Rect target )
	{
		synchronized(lockObj)
		{
			if (target == null)
			{
				targetRectangles = null;
				return;
			}

			if (targetRectangles == null) targetRectangles = new ArrayList<Rect>();

			targetRectangles.add(target);
		}
	}

	@Override
	public void addTarget( Rect target, int width, Scalar color )
	{
		targetWidth = width;
		targetColor = color;

		addTarget(target);
	}

	@Override
	public void setScale( int width, int height )
	{
		scale = "scale="+width+":"+height;
	}

	private void deleteFolder(Context context, String folder){
		String internalPath = context.getExternalFilesDirs(null)[0].getAbsolutePath();
		File imageDir = new File(internalPath,folder);
		if (imageDir.isDirectory()) {
			String[] files = imageDir.list();
			for (String file : files) {
				new File(imageDir, file).delete();
			}
		}
	}

	public ArrayList<Rect> getTarget(){
		return targetRectangles;
	}


	//<editor-fold desc="Unused functions">
	@Override
	public boolean isRecording()
	{
		return recording;
	}

	public Size getImageSize()
	{
		synchronized(lockObj)
		{
			if (image == null) return new Size(0,0);

			return new Size(image.width(), image.height());
		}
	}

	@Override
	public void setContours(ArrayList<MatOfPoint> contours)
	{
		synchronized(lockObj) {this.contours = contours;}
	}

	@Override
	public void setContours( ArrayList<MatOfPoint> contours, int width, Scalar color )
	{
		contourWidth = width;
		//contourColor = color;

		setContours(contours);
	}

	@Override
	public void setStatusBar( String message )
	{
		synchronized(lockObj) {statusBar = message;}
	}

	@Override
	public void setStatusBar( Supplier<String> method )
	{
		synchronized(lockObj) {statusBarMethod = method;}
	}

	//</editor-fold>
}
